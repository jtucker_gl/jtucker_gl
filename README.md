# Hi, I'm Jeff 👋

I have over 15 years of experience across product, design, and engineering roles. I am currently
the [Senior Product Manager](https://handbook.gitlab.com/job-families/product/product-manager/#senior-product-manager) for the [Manage:Foundations](https://about.gitlab.com/direction/manage/foundations/) team at GitLab.